<?php
header('Content-Type: application/json');
$messages=[
    array('IDMessage' => 1, 'Date' => 'Senin, 01 Januari 2018',"Message" => "ini pesan", "Status" => 3),
    array('IDMessage' => 2, 'Date' => 'Selasa, 02 Januari 2018',"Message" => "anda ada pesan", "Status" => 2),
    array('IDMessage' => 3, 'Date' => 'Rabu, 03 Januari 2018',"Message" => "tes pesan", "Status" => 1),
    array('IDMessage' => 4, 'Date' => 'Kamis, 04 Januari 2018',"Message" => "segera dikumpulkan", "Status" => 3),
    array('IDMessage' => 5, 'Date' => 'Jumat, 05 Januari 2018',"Message" => "anda bosan ya", "Status" => 2),
    array('IDMessage' => 6, 'Date' => 'Sabtu, 06 Januari 2018',"Message" => "tes segera dimulai", "Status" => 1),
];

$myJSON = json_encode($messages);

echo $myJSON;
?>
