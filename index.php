
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://code.highcharts.com/highcharts.js"></script>

    <script>
    function searchNotification() {
        // var input, filter, ul, li, a, i, txtValue,stats,s;
        // var checkboxes = document.getElementsByName('customRadioInline1');
        // var vals = "";
        // for (var i=0, n=checkboxes.length;i<n;i++)
        // {
        //     if (checkboxes[i].checked)
        //     {
        //         vals += ","+checkboxes[i].value;
        //     }
        // }
        // if (vals) vals = vals.substring(1);
        // input = document.getElementById("searchNotif");
        // filter = input.value.toUpperCase();
        // ul = document.getElementById("listNotif");
        // li = listNotif.getElementsByTagName("li");
        //
        // for (i = 0; i < li.length; i++) {
        //     a = li[i].getElementsByTagName("p")[0];
        //     s = li[i].getElementsByTagName("span")[0];
        //     txtValue = a.textContent || a.innerText;
        //     stats = s.textContent || s.innerText;
        //     if (txtValue.toUpperCase().indexOf(filter) > -1) {
        //         li[i].style.display = "";
        //     } else {
        //         li[i].style.display = "hidden";
        //     }
        // }
        //   console.log(li.length);

      }
        function tampilkanwaktu(){
           var waktu = new Date();
           var sh = waktu.getHours() + "";
           var sm = waktu.getMinutes() + "";
           var ss = waktu.getSeconds() + "";
           document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
       }
    function close_window(){
      close();
    }
    </script>
    <title>Bigbox</title>

  </head>
  <body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">
    <div class="container-fluid" style="padding:20px;margin-bottom:50px;">
    <!-- Row 1 -->
    <div class="row">

        <div class="col-sm-4">
            <div class="card">
                <div class="card-body" >
                <div class="header-profile row">
                <img src="icon.png" class="rounded" width="80px" height="80px">
                <div class="col" style="margin-top:15px">
                <h5 >Mirza Ukasyah Yazdani</h5>
                <h6>mirza@mail.com</h6>
                </div>

                </div>
                <button type="button" class="btn btn-outline-primary btn-block"><span class="badge badge-danger badge-pill" id="notif"></span>  Notifications</button>
                <button type="button" class="btn btn-outline-success btn-block"><span class="badge badge-danger badge-pill" id="inbox"></span>  Inbox</button>
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#exampleModal">EXIT</button>
                </div>
            </div>
            <div class="card" style="margin-top:15px;">
            <h6 class="header-text" style="margin:10px;">DAILY PERFORMANCE</h6>
                <div class="card-body" >
                <div class="btn-group btn-group-sm ">
                <button type="button" class="btn btn-outline-primary" onclick="chartfunc(1)">COLUMN</button>
                <button type="button" class="btn btn-outline-primary" onclick="chartfunc(2)">PIE</button>
                </div>
                    <div id="chart0" style="height:300px;">
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-8">
        <div class="marquee">
        <h3 class=""><i>Assisting Your Bussiness From Heart :: 03 Agustus 2018 Gathering Bigbox</i></h3>
        </div>
        <div class="card" >
                <div class="card-body">
                <h6 class="header-text">DAILY PERFORMANCE</h6>
                <table id="table1" class="display" style="width:100%;">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Target Time (Hour)</th>
                            <th>Work Time (Hour)</th>
                            <th>Achievement (Percentage)</th>
                            <th>Overtime (Hour)</th>
                            <th>Day Name</th>
                        </tr>
                    </thead>
                    <tbody id="tdaily">

                    </tbody>
                        <tfoot>
                            <tr>
                              <th>Date</th>
                              <th>Target Time (Hour)</th>
                              <th>Work Time (Hour)</th>
                              <th>Achievement (Percentage)</th>
                              <th>Overtime (Hour)</th>
                              <th>Day Name</th>
                            </tr>
                        </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Row 2 -->
    <div class="row">
        <div class="col-sm-12">
        <div class="card" style="margin-top:20px;">
                <div class="card-body">
                <div class="row">
                <div class="col-sm-8" style="margin-bottom:15px;">
                <div class="row">

                <div class="form-group col-sm-3">
                    <label for="usr">STARTDATE:</label>
                    <select class="form-control" id="start">
                      <?php
                        for ($i=1; $i <=30 ; $i++) {
                          echo '<option>'.$i.'</option>';
                        }
                       ?>
                    </select>
                </div>

                <div class="form-group col-sm-3">
                    <label for="pwd">ENDDATE:</label>
                    <select class="form-control" id="end">
                        <?php
                          for ($i=1; $i <=30 ; $i++) {
                            echo '<option>'.$i.'</option>';
                          }
                         ?>
                    </select>
                </div>
                <div class="form-group col-sm-3">
                    <label for="pwd">Month:</label>
                    <select class="form-control" id="monthval">
                        <?php
                          for ($i=1; $i <=12 ; $i++) {
                            echo '<option>'.$i.'</option>';
                          }
                         ?>
                    </select>
                </div>
                <div class="col" style="margin-top:30px;">
                <button type="button" class="btn btn-primary" style="width:90px;">Reload</button></div>
                </div>
                </div>
                <div class="col-sm-4"><h6 class="header-text">YEAR SUMMARY PERFORMANCE</h6></div>
                </div>

                <table id="table2" class="display" style="width:100%">
                  <thead>
                      <tr>
                          <th>Date</th>
                          <th>Month</th>
                          <th>Target Time (Hour)</th>
                          <th>Work Time (Hour)</th>
                          <th>Achievement (Percentage)</th>
                          <th>Overtime (Hour)</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tbody id="tyear">


                  </tbody>
                      <tfoot>
                          <tr>
                            <th>Date</th>
                            <th>Month</th>
                            <th>Target Time (Hour)</th>
                            <th>Work Time (Hour)</th>
                            <th>Achievement (Percentage)</th>
                            <th>Overtime (Hour)</th>
                            <th></th>
                          </tr>
                      </tfoot>
                </table>
                </div>
            </div>
        </div>
    </div>
     <!-- Row 3 -->
    <div class="row">
        <div class="col-sm-12">
        <div class="card" style="margin-top:20px;">


                <div class="card-body">
                <h6 style="color:#FF0000;text-align:right;">YEAR SUMMARY PERFORMANCE</h6>
                <div class="row">
                <div class="card col" style="margin:10px"><div class="card-body"><div id="chart1" ></div></div></div>
                <div class="card col" style="margin:10px"><div class="card-body"><div id="chart2" ></div></div></div>
                <div class="card col" style="margin:10px"><div class="card-body"><div id="chart3" ></div></div></div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Row 4 -->
    <div class="row">
        <div class="col-sm-6">
          <div class="card" style="margin-top:20px;">

              <div class="card-body">
                  <div class="container-mt3">
                      <h5 style="text-align:center;margin-bottom:20px;">NOTIFICATION</h5>
                      <div class="row">
                      <div class="form-group col-sm-6">
                          <input type="text" class="form-control" id="searchNotif" placeholder="Search..">
                      </div >
                      <div class="form-group col-sm-6">
                      <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="radio" id="cbDanger" name="customRadioInline1" class="custom-control-input" value="1">
                        <label class="custom-control-label text-danger" for="cbDanger">Danger</label>
                      </div>
                      <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="radio" id="cbWarning" name="customRadioInline1" class="custom-control-input bg-warning" value="2">
                        <label class="custom-control-label text-warning" for="cbWarning">Warning</label>
                      </div>
                      <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="radio" id="cbSafe" name="customRadioInline1" class="custom-control-input" value="3">
                        <label class="custom-control-label text-success" for="cbSafe">Safe</label>
                      </div>
                      </div>
                    </div>
                      <ul class="list-group list-wrap" id="listNotif">
                      </ul>
                  </div>
              </div>
          </div>
        </div>
        <div class="col-sm-6">
            <div class="card" style="margin-top:20px;">

                <div class="card-body">
                    <div class="container-mt3">
                        <h5 style="text-align:center;margin-bottom:20px;">INBOX</h5>
                        <div class="form-group ">
                            <input type="text" class="form-control" id="searchInbox" placeholder="Search..">
                        </div>
                        <ul class="list-group list-wrap" id="listInbox">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <footer class="footer">
      <div class="row">
        <span class="footer-text col-sm-8">TECH MAYANTARA ASIA</span>
        <div class="col-sm-3">
        <span class="badge badge-primary badge-pill footer-badge" id="notif1" style="margin-left:80%;"></span>
        <span class="badge badge-success badge-pill footer-badge" id="inbox1"></span>
        <h6 class="footer-right" id="date"><?php echo date("d F Y"); ?><br><span id="clock"></span> </h6>
        </div>
      </div>
    </footer>

    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmation </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Are you sure to exit this web page ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-danger" onclick="close_window();return false;">Exit</button>
      </div>
    </div>
  </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
<script>
    var listNotif = document.getElementById('listNotif');
    var listInbox = document.getElementById('listInbox');
    var notifCount = document.getElementById('notif');
    var inboxCount = document.getElementById('inbox');
    var notifCount1 = document.getElementById('notif1');
    var inboxCount1 = document.getElementById('inbox1');
    var tdaily = document.getElementById('tdaily');
    var tyear = document.getElementById('tyear');
    var messages,performances;
    var currentYear=[];var currentMonth=[];
    var dtnow = new Date();
    var draw = false;

    $.ajax({
      type:"GET",
      url: "http://localhost/BIGBOX/inbox.php",
      dataType: 'json',
      async: false,
      success: function(data) {
        messages = data;
      }
    });
    $.ajax({
      type:"GET",
      url: "http://localhost/BIGBOX/performance.php",
      dataType: 'json',
      async: false,
      success: function(data) {
        performances = data;
      }
    });
    notifCount.appendChild(document.createTextNode(messages.length));
    inboxCount.appendChild(document.createTextNode(messages.length));
    notifCount1.appendChild(document.createTextNode(messages.length));
    inboxCount1.appendChild(document.createTextNode(messages.length));
    for (var i = 0; i < messages.length; i++) {
      var li = document.createElement('li');
       if(messages[i].Status == 1){
         li.className = 'list-group-item justify-content-between align-items-center list-group-item-danger';
         li.innerHTML = messages[i].Date + '<br>'+messages[i].Message+'</p>' + '<span hidden>'+messages[i].Status+'</span>';
         listNotif.appendChild(li)
       }else if(messages[i].Status == 2){
         li.className = 'list-group-item  justify-content-between align-items-center list-group-item-warning';
         li.innerHTML = messages[i].Date + '<br>'+ messages[i].Message + '<span hidden>'+messages[i].Status+'</span>';
         listNotif.appendChild(li)
       }else if(messages[i].Status == 3){
         li.className = 'list-group-item  justify-content-between align-items-center list-group-item-success';
         li.innerHTML = messages[i].Date + '<br>'+ messages[i].Message + '<span hidden>'+messages[i].Status+'</span>';
         listNotif.appendChild(li)
       }
    }

    for (var i = 0; i < messages.length; i++) {
      var li = document.createElement('li');
      li.className = 'list-group-item justify-content-between align-items-center';
      li.innerHTML = messages[i].Date + '<br>'+ messages[i].Message;
      listInbox.appendChild(li);
    }

    for (var i = 0; i < performances.length; i++) {
        var rawdate = performances[i].Date.split("-");
        var date = new Date(rawdate[2], rawdate[1] - 1, rawdate[0]);
        if(date.getFullYear() == dtnow.getFullYear()){
            currentYear.push(performances[i]);
            if ((date.getMonth()+1) == (dtnow.getMonth()+1)) {
                currentMonth.push(performances[i]);
            }
        }
    }

    var monthChartData = {
          chart: {
             events: {
                  drilldown: function (e) {
                      if (!e.seriesOptions) {

                          var chart = this;



                          // Show the loading label
                          chart.showLoading('Loading ...');

                          setTimeout(function () {
                              chart.hideLoading();
                              chart.addSeriesAsDrilldown(e.point, series);
                          }, 1000);
                      }

                  }
              },
              plotBorderWidth: 0
          },

          title: {
              text: 'Daily Perfomance',
          },
          //
          subtitle: {
                  text: 'Subtitle'
          },
          //
          xAxis: {
                categories: ['TargetTime (Hour)', 'WorkTime (Hour)', 'Achievement (%)','Overtime (Hour)'],
          },
          //
          yAxis: {

                  title: {
                      margin: 10,
                      text: 'Data Value'
                  },
          },
          //
          legend: {
              enabled: true,
          },
          //
          plotOptions: {
              series: {
                  pointPadding: 0.2,
                  borderWidth: 0,
                  dataLabels: {
                      enabled: true
                  }
              },
              pie: {
                  plotBorderWidth: 0,
                  allowPointSelect: true,
                  cursor: 'pointer',
                  size: '100%',
                  dataLabels: {
                      enabled: true,
                      format: '{point.name}: <b>{point.y}</b>'
                  }
              }
          },
          //
           series: [{
              name: 'Day -',
              colorByPoint: true,
              data: [1,1,1,1]
              // data: [1, 2, 3, 4]
          }],
          //
          drilldown: {
              series: []
          }
      };
    var yearChartData = {
            chart: {
               events: {
                    drilldown: function (e) {
                        if (!e.seriesOptions) {

                            var chart = this;



                            // Show the loading label
                            chart.showLoading('Loading ...');

                            setTimeout(function () {
                                chart.hideLoading();
                                chart.addSeriesAsDrilldown(e.point, series);
                            }, 1000);
                        }

                    }
                },
                plotBorderWidth: 0
            },

            title: {
                text: 'Year Perfomance',
            },
            //
            subtitle: {
                    text: 'Subtitle'
            },
            //
            xAxis: {
                  categories: ['TargetTime (Hour)', 'WorkTime (Hour)', 'Achievement (%)','Overtime (Hour)'],
            },
            //
            yAxis: {

                    title: {
                        margin: 10,
                        text: 'Data Value'
                    },
            },
            //
            legend: {
                enabled: true,
            },
            //
            plotOptions: {
                series: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true
                    },
                    point: {
                    	events: {
                      	click: function(e){
                        	var seriesName = e.point.series.name;
                          var day = seriesName.substring(6,seriesName.length);
                          $.fn.dataTable.ext.search.push(
                              function( settings, data, dataIndex ) {
                                  var min = parseInt( $('#start').val(), 10 );
                                  var max = parseInt( $('#end').val(), 10 );
                                  var date = parseFloat( data[0] ) || 0;

                                  if ( (( isNaN( min ) && isNaN( max ) ) ||
                                       ( isNaN( min ) && date <= max ) ||
                                       ( min <= date   && isNaN( max ) ) ||
                                       ( min <= date   && date <= max ) ) && (day == date) )
                                  {
                                      return true;
                                  }
                                  return false;
                              }
                          );
                        }
                      }
                }
                },
                pie: {
                    plotBorderWidth: 0,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    size: '100%',
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: <b>{point.y}</b>'
                    }
                }
            },
            //
             series: [{
                name: 'Day -',
                colorByPoint: true,
                data: [1,1,1,1]
                // data: [1, 2, 3, 4]
            }],
            //
            drilldown: {
                series: []
            }
        };
    initTableMonth();


    $(document).ready(function(){

        $("#searchInbox").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          console.log(value);
          $("#listInbox li").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });

       $("#searchNotif").on("keyup", function() {
         var value = $(this).val().toLowerCase();
         $("#listNotif li").filter(function() {
           $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
         });
       });
       initTableYear();
     });

    function initTableMonth() {
      var t =  $('#table1').DataTable({
        "pageLength": 1
      });
       for (var i = 0; i < currentMonth.length; i++) {
         var target = currentMonth[i];
           var rawdate = target.Date.split("-");
           var date = new Date(rawdate[2], rawdate[1] - 1, rawdate[0]);
         t.row.add([
           date.getDate(),
           target.TargetTime,
           target.WorkTime,
           target.Achievement,
           target.Overtime,
           target.DayName,
         ]).draw();
       }
      var dataTable = getTableData(t);
      createChart(dataTable);
      setTableEvents(t);
    }

    function getTableData(table) {
      var dataArray = [],
      DayArr = [],
      TargetTimeArr = [],
      WorkTimeArr = [],
      AchievementArr = [],
      OvertimeArr = [];

      // loop table rows
      table.rows({ search: "applied" }).every(function() {
        var data = this.data();
        DayArr.push(data[0]);
        TargetTimeArr.push(data[1]);
        WorkTimeArr.push(data[2]);
        AchievementArr.push(data[3]);
        OvertimeArr.push(data[4]);
      });

      // store all data in dataArray
      dataArray.push(DayArr,TargetTimeArr, WorkTimeArr, AchievementArr,OvertimeArr);

      return dataArray;
    }

    function setTableEvents(table) {
      // listen for page clicks
      table.on("page", () => {
        draw = true;
      });

      // listen for updates and adjust the chart accordingly
      table.on("draw", () => {
        if (draw) {
          draw = false;
        } else {
          var tableData = getTableData(table);
          createChart(tableData);
        }
      });
    }

    function createChart(data){

      // Column chart
      monthChartData.chart.renderTo = 'chart0';
      monthChartData.chart.type = 'column';
      monthChartData.subtitle.text = 'Day '+ data[0][0];
      monthChartData.series = [{
        name: 'Day ' + data[0][0],
        colorByPoint: true,
        data: [data[1][0], data[2][0], data[3][0], data[4][0]]
     }];
      var chart1 = new Highcharts.Chart(monthChartData);
    }

    function chartfunc(btStats){

      if(btStats == 1)
          {

              monthChartData.chart.renderTo = 'chart0';
              monthChartData.chart.type = 'column';
              var chart1 = new Highcharts.Chart(monthChartData);
          }
      else if(btStats == 2)
          {
              monthChartData.chart.renderTo = 'chart0';
              monthChartData.chart.type = 'pie';
              var chart1 = new Highcharts.Chart(monthChartData);
          }
      else
          {
              monthChartData.chart.renderTo = 'chart0';
              monthChartData.chart.type = 'line';
              var chart1 = new Highcharts.Chart(monthChartData);
          }

    }
    // Year Section
    function initTableYear(){
      var t =  $('#table2').DataTable();
      $('#start, #end,#monthval').change( function() {
          t.draw();
      } );
       for (var i = 0; i < currentYear.length; i++) {
          var target = currentYear[i];
          var rawdate = target.Date.split("-");
           var date = new Date(rawdate[2], rawdate[1] - 1, rawdate[0]);
         t.row.add([
           date.getDate(),
           date.getMonth()+1,
           target.TargetTime,
           target.WorkTime,
           target.Achievement,
           target.Overtime,
           target.DayName,
         ]).draw();
       }
      var dataTable = getTableData2(t);
      console.log(dataTable);
      createColumnChartYear(dataTable);
      createPieChartYear(dataTable);
      createAreaChartYear(dataTable);
      setYearTableEvents(t);
    }

    function getTableData2(table) {
      var dataArray = [],
      DayArr = [],
      MonthArr = [],
      TargetTimeArr = [],
      WorkTimeArr = [],
      AchievementArr = [],
      OvertimeArr = [];

      // loop table rows
      table.rows({ search: "applied" }).every(function() {
        var data = this.data();
        DayArr.push(data[0]);
        MonthArr.push(data[1]);
        TargetTimeArr.push(data[2]);
        WorkTimeArr.push(data[3]);
        AchievementArr.push(data[4]);
        OvertimeArr.push(data[5]);
      });

      // store all data in dataArray
      dataArray.push(DayArr,MonthArr,TargetTimeArr, WorkTimeArr, AchievementArr,OvertimeArr);

      return dataArray;
    }

    function createColumnChartYear(data){

      var arrSeries = [];
      for (var i = 0; i < data.length; i++) {
        if(data[0][i] != null){
        arrSeries.push({
          name : 'Day - ' +data[0][i] ,
          colorByPoint : true,
          data : [data[2][i], data[3][i], data[4][i], data[5][i]]
        });
      }
      }
      yearChartData.chart.renderTo = 'chart1';
      yearChartData.chart.type = 'column';
      yearChartData.subtitle.text = 'Month ' + data[1][0];
      yearChartData.series = arrSeries;
      var chart1 = new Highcharts.Chart(yearChartData);
    }
    function createPieChartYear(data){
      var arrSeries = [];
      for (var i = 0; i < data.length; i++) {
        if(data[0][i] != null){
        arrSeries.push({
          name : 'Day - ' +data[0][i] ,
          colorByPoint : true,
          data : [data[2][i], data[3][i], data[4][i], data[5][i]]
        });
      }
      }
      yearChartData.chart.renderTo = 'chart2';
      yearChartData.chart.type = 'pie';
      yearChartData.subtitle.text = 'Month ' + data[1][0];
      yearChartData.series = arrSeries;
      var chart2 = new Highcharts.Chart(yearChartData);
    }
    function createAreaChartYear(data){
      var arrSeries = [];
      for (var i = 0; i < data.length; i++) {
        if(data[0][i] != null){
        arrSeries.push({
          name : 'Day - ' +data[0][i] ,
          colorByPoint : true,
          data : [data[2][i], data[3][i], data[4][i], data[5][i]]
        });
      }
      }
      yearChartData.chart.renderTo = 'chart3';
      yearChartData.chart.type = 'area';
      yearChartData.subtitle.text = 'Month ' + data[1][0];
      yearChartData.series = arrSeries;
      var chart3 = new Highcharts.Chart(yearChartData);
    }

    function setYearTableEvents(table) {
      // listen for page clicks
      table.on("page", () => {
        draw = true;
      });

      // listen for updates and adjust the chart accordingly
      table.on("draw", () => {
        if (draw) {
          draw = false;
        } else {
          var tableData = getTableData2(table);
          createColumnChartYear(tableData);
          createPieChartYear(tableData);
          createAreaChartYear(tableData);
        }
      });
      }

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var min = parseInt( $('#start').val(), 10 );
            var max = parseInt( $('#end').val(), 10 );
            var monthval = parseInt( $('#monthval').val(), 10 );
            var date = parseFloat( data[0] ) || 0;
            var month = parseFloat( data[1] ) || 0;

            if ( (( isNaN( min ) && isNaN( max ) ) ||
                 ( isNaN( min ) && date <= max ) ||
                 ( min <= date   && isNaN( max ) ) ||
                 ( min <= date   && date <= max ) ) && (monthval == month) )
            {
                return true;
            }
            return false;
        }
    );

</script>
