<?php
header('Content-Type: application/json');
$performances=[
    array('IDPerformance' => 1, 'Date' => "05-03-2018","TargetTime" => 9,"WorkTime" => 8, "Achievement" => 20,"Overtime" => 1,"DayName" => "Monday"),
    array('IDPerformance' => 2, 'Date' => "06-03-2018","TargetTime" => 8,"WorkTime" => 7, "Achievement" => 100,"Overtime" => 1,"DayName" => "Tuesday"),
    array('IDPerformance' => 3, 'Date' => "07-03-2018","TargetTime" => 6,"WorkTime" => 3, "Achievement" => 80,"Overtime" => 1,"DayName" => "Wednesday"),
    array('IDPerformance' => 4, 'Date' => "08-03-2018","TargetTime" => 3,"WorkTime" => 2, "Achievement" => 90,"Overtime" => 1,"DayName" => "Thursday"),
    array('IDPerformance' => 5, 'Date' => "09-03-2018","TargetTime" => 4,"WorkTime" => 7, "Achievement" => 50,"Overtime" => 1,"DayName" => "Friday"),

    array('IDPerformance' => 6, 'Date' => "10-04-2019","TargetTime" => 7,"WorkTime" => 10, "Achievement" => 90,"Overtime" => 1,"DayName" => "Saturday"),
    array('IDPerformance' => 7, 'Date' => "11-04-2019","TargetTime" => 9,"WorkTime" => 8, "Achievement" => 80,"Overtime" => 1,"DayName" => "Sunday"),
    array('IDPerformance' => 8, 'Date' => "12-04-2019","TargetTime" => 6,"WorkTime" => 8, "Achievement" => 75,"Overtime" => 1,"DayName" => "Monday"),
    array('IDPerformance' => 9, 'Date' => "13-04-2019","TargetTime" => 2,"WorkTime" => 6, "Achievement" => 65,"Overtime" => 1,"DayName" => "Tuesday"),

    array('IDPerformance' => 10, 'Date' => "14-05-2017","TargetTime" => 10,"WorkTime" => 10, "Achievement" => 60,"Overtime" => 1,"DayName" => "Wednesday"),
    array('IDPerformance' => 11, 'Date' => "15-05-2017","TargetTime" => 11,"WorkTime" => 12, "Achievement" => 20,"Overtime" => 1,"DayName" => "Thursday"),
    array('IDPerformance' => 12, 'Date' => "16-05-2017","TargetTime" => 9,"WorkTime" => 9, "Achievement" => 40,"Overtime" => 1,"DayName" => "Friday"),
    array('IDPerformance' => 13, 'Date' => "17-05-2017","TargetTime" => 2,"WorkTime" => 3, "Achievement" => 45,"Overtime" => 1,"DayName" => "Saturday"),
    array('IDPerformance' => 14, 'Date' => "18-05-2017","TargetTime" => 9,"WorkTime" => 8, "Achievement" => 80,"Overtime" => 1,"DayName" => "Sunday"),
    array('IDPerformance' => 15, 'Date' => date("d-m-Y"),"TargetTime" => 6,"WorkTime" => 8, "Achievement" => 75,"Overtime" => 1,"DayName" => "Monday"),
    array('IDPerformance' => 16, 'Date' => "19-06-2019","TargetTime" => 2,"WorkTime" => 6, "Achievement" => 65,"Overtime" => 1,"DayName" => "Tuesday"),
    array('IDPerformance' => 17, 'Date' => "20-06-2019","TargetTime" => 10,"WorkTime" => 10, "Achievement" => 60,"Overtime" => 1,"DayName" => "Wednesday"),
    array('IDPerformance' => 18, 'Date' => "21-06-2019","TargetTime" => 11,"WorkTime" => 12, "Achievement" => 20,"Overtime" => 1,"DayName" => "Thursday"),
    array('IDPerformance' => 19, 'Date' => "22-06-2019","TargetTime" => 9,"WorkTime" => 9, "Achievement" => 40,"Overtime" => 1,"DayName" => "Friday"),
    array('IDPerformance' => 20, 'Date' => "23-06-2019","TargetTime" => 2,"WorkTime" => 3, "Achievement" => 45,"Overtime" => 1,"DayName" => "Saturday"),
    array('IDPerformance' => 21, 'Date' => "24-07-2019","TargetTime" => 9,"WorkTime" => 8, "Achievement" => 80,"Overtime" => 1,"DayName" => "Sunday"),
    array('IDPerformance' => 22, 'Date' => "25-07-2019","TargetTime" => 6,"WorkTime" => 8, "Achievement" => 75,"Overtime" => 1,"DayName" => "Monday"),
    array('IDPerformance' => 23, 'Date' => "26-07-2019","TargetTime" => 2,"WorkTime" => 6, "Achievement" => 65,"Overtime" => 1,"DayName" => "Tuesday"),
    array('IDPerformance' => 24, 'Date' => "27-07-2019","TargetTime" => 10,"WorkTime" => 10, "Achievement" => 60,"Overtime" => 1,"DayName" => "Wednesday"),
    array('IDPerformance' => 25, 'Date' => "28-07-2019","TargetTime" => 11,"WorkTime" => 12, "Achievement" => 20,"Overtime" => 1,"DayName" => "Thursday"),
    array('IDPerformance' => 26, 'Date' => "29-08-2019","TargetTime" => 9,"WorkTime" => 9, "Achievement" => 40,"Overtime" => 1,"DayName" => "Friday"),
    array('IDPerformance' => 27, 'Date' => "01-01-2019","TargetTime" => 2,"WorkTime" => 3, "Achievement" => 45,"Overtime" => 1,"DayName" => "Saturday"),
    array('IDPerformance' => 28, 'Date' => "02-08-2019","TargetTime" => 2,"WorkTime" => 3, "Achievement" => 45,"Overtime" => 1,"DayName" => "Sunday"),
    array('IDPerformance' => 29, 'Date' => "03-08-2019","TargetTime" => 2,"WorkTime" => 3, "Achievement" => 45,"Overtime" => 1,"DayName" => "Monday"),
    array('IDPerformance' => 30, 'Date' => "04-08-2019","TargetTime" => 2,"WorkTime" => 3, "Achievement" => 45,"Overtime" => 1,"DayName" => "Tuesday"),
];

$myJSON = json_encode($performances);

echo $myJSON;
?>
